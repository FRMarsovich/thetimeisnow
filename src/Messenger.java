public class Messenger implements Runnable {
    private int time;
    public Chronometr ch;
    public static boolean finish=false;
    public boolean flag=true;
    Messenger(int time, Chronometr ch) {
        this.time=time;
        this.ch=ch;
    }
    public void waitForTime() {
        while(true){
        synchronized (ch) {
            try{
                while (flag)
                    ch.wait();
                if (finish) return;
                if(ch.time%this.time==0)
                {
                    System.out.println("Прошло " + this.time + " секунд");
                    flag=true;
                }
                flag=true;
            }
            catch(InterruptedException e) {}
        }}
    }
    public void run()  {
        waitForTime();
        System.out.println("Ты кое-что потерял. Знаешь что именно? Ты потерял минуту своей жизнь! (:");
        }
}
